#include "main.h"
#include "snd/psg/sfxbank.h"

////////////////////////////////////////////////////////////////////////////////
//
// int main()
//
////////////////////////////////////////////////////////////////////////////////
int main(){
	
	u16 keys = 0, keyPress = 1;
	
	VDP_regInit();
	
	// modo de 32 columnas
	VDP_setReg(12, 0x00 );
	
	// cambia el color del fondo
    VDP_setReg( 7, 0x0F );
	
	// carga el driver XGM en el z80
    SND_loadDriver_XGM();
	
	// inicio el banco de sonidos psg
    psgFxInit(sfxbank_data);
	
    VDP_loadTileData( font00_tiles, pos_vram_font, font_tiles_size, 1);
	
	VDP_drawText("sonido",BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 13, 8 );
	
	VDP_setPalette((u16 *)font00_pal, 0, 16);
	
	SND_loadSong_XGM(0);
	
	//XGM_setMusicTempo(50);

    ////////////////////////////////////////
    //  bucle infinito
	while(1){
		
		// lee el pad
		keys = get_pad(0) & SEGA_CTRL_BUTTONS;

		if( keys & SEGA_CTRL_RIGHT && !keyPress  ){
			keyPress = 1;
			psgFxPlay(2);
		}
		
		if( keys & SEGA_CTRL_LEFT && !keyPress  ){
			keyPress = 1;
			SND_loadPcm_XGM(0,1);
		}
		
		if( !(keys & SEGA_CTRL_RIGHT) &&  !(keys & SEGA_CTRL_LEFT) )
			keyPress = 0;
		
		VDP_waitVSync();
	}

    return 0;
}
