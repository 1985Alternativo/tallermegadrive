#include "main.h"
#include "snd/psg/sfxbank.h"

////////////////////////////////////////////////////////////////////////////////
//
// int main()
//
////////////////////////////////////////////////////////////////////////////////
int main(){
	
	u16 keys = 0;
	
	VDP_regInit();
	
	// modo de 32 columnas
	VDP_setReg(12, 0x00 );
	
	// cambia el color del fondo
    VDP_setReg( 7, 0x0F );
	
	// carga el driver XGM en el z80
    SND_loadDriver_XGM();
	
    VDP_loadTileData( font00_tiles, pos_vram_font, font_tiles_size, 1);
	
	VDP_drawText("controles",BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 11, 8 );
	
	VDP_drawText("ARRIBA: 0",BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 11, 10 );
	VDP_drawText("ABAJO : 0",BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 11, 11 );
	VDP_drawText("IZQU. : 0",BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 11, 12 );
	VDP_drawText("DCHA. : 0",BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 11, 13 );
	VDP_drawText("START : 0",BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 11, 14 );
	VDP_drawText("A     : 0",BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 11, 15 );
	VDP_drawText("B     : 0",BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 11, 16 );
	VDP_drawText("C     : 0",BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 11, 17 );
	VDP_drawText("X     : 0",BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 11, 18 );
	VDP_drawText("Y     : 0",BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 11, 19 );
	VDP_drawText("Z     : 0",BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 11, 20 );
	
	VDP_setPalette((u16 *)font00_pal, 0, 16);

    ////////////////////////////////////////
    //  bucle infinito
	while(1){
		
		// lee el pad
		keys = get_pad(0) & SEGA_CTRL_BUTTONS;

		VDP_drawNumber( (  keys & SEGA_CTRL_UP ) ? 1 : 0, BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 19, 10 );
		VDP_drawNumber( (  keys & SEGA_CTRL_DOWN ) ? 1 : 0, BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 19, 11 );
		VDP_drawNumber( (  keys & SEGA_CTRL_LEFT ) ? 1 : 0, BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 19, 12 );
		VDP_drawNumber( (  keys & SEGA_CTRL_RIGHT ) ? 1 : 0, BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 19, 13 );
		VDP_drawNumber( (  keys & SEGA_CTRL_START ) ? 1 : 0, BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 19, 14 );
		VDP_drawNumber( (  keys & SEGA_CTRL_A ) ? 1 : 0, BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 19, 15 );
		VDP_drawNumber( (  keys & SEGA_CTRL_B ) ? 1 : 0, BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 19, 16 );
		VDP_drawNumber( (  keys & SEGA_CTRL_C ) ? 1 : 0, BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 19, 17 );
		VDP_drawNumber( (  keys & SEGA_CTRL_X ) ? 1 : 0, BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 19, 18 );
		VDP_drawNumber( (  keys & SEGA_CTRL_Y ) ? 1 : 0, BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 19, 19 );
		VDP_drawNumber( (  keys & SEGA_CTRL_Z ) ? 1 : 0, BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 19, 20 );
		
		VDP_waitVSync();
	}

    return 0;
}
