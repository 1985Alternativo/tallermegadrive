#include "main.h"
#include "snd/psg/sfxbank.h"

////////////////////////////////////////////////////////////////////////////////
//
// int main()
//
////////////////////////////////////////////////////////////////////////////////
int main(){
	
    VDP_loadTileData( font00_tiles, pos_vram_font, font_tiles_size, 1);
	
	VDP_drawText("dale FRAAAN",BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 15, 24 );
	
	VDP_setPalette((u16 *)font00_pal, 0, 16);

    ////////////////////////////////////////
    //  bucle infinito
	while(1){
		VDP_waitVSync();
	}

    return 0;
}
