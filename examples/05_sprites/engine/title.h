#ifndef _TITLE_H_
#define _TITLE_H_

void draw1985Logo();
u8 recordsScreen( u8 numPlayers );
u8 titleScreen();
void creditsScreen();
u8 introScreen();

#endif
