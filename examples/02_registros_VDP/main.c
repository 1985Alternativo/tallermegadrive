#include "main.h"
#include "snd/psg/sfxbank.h"

////////////////////////////////////////////////////////////////////////////////
//
// int main()
//
////////////////////////////////////////////////////////////////////////////////
int main(){
	
	// inicializamos los registros
	VDP_regInit();
	
	// modo de 32 columnas
	VDP_setReg(12, 0x00 );
	
	// cambia el color del fondo
    VDP_setReg( 7, 0x0F );
	
	// carga el driver XGM en el z80
    SND_loadDriver_XGM();
	
    VDP_loadTileData( font00_tiles, pos_vram_font, font_tiles_size, 1);
	
	VDP_drawText("registros VDP",BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 8, 8 );
	
	VDP_setPalette((u16 *)font00_pal, 0, 16);

    ////////////////////////////////////////
    //  bucle infinito
	while(1){
		VDP_waitVSync();
	}

    return 0;
}
