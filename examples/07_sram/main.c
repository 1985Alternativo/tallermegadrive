#include "main.h"
#include "snd/psg/sfxbank.h"

////////////////////////////////////////////////////////////////////////////////
//
// int main()
//
////////////////////////////////////////////////////////////////////////////////
int main(){
	
	u8 a;
	
	VDP_regInit();
	
	// modo de 32 columnas
	VDP_setReg(12, 0x00 );
	
	// cambia el color del fondo
    VDP_setReg( 7, 0x0F );
	
	// carga el driver XGM en el z80
    SND_loadDriver_XGM();
	
	// inicia la SRAM
    initSRAM();
	
    VDP_loadTileData( font00_tiles, pos_vram_font, font_tiles_size, 1);
	
	VDP_drawText("sram",BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 11, 5 );
	
	VDP_drawNumber( num_executions, BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 17, 5 );
	
	 for( a = 0; a < 5; a++ )
        VDP_drawText (namesTable[0][a],BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 11, 7+2*a );

	/*strcpy(namesTable[0][0], "PIXELARTM");
	
	SRAM_enable();
    writeRecordsTableSRAM();
    SRAM_disable();*/
	
	VDP_setPalette((u16 *)font00_pal, 0, 16);

    ////////////////////////////////////////
    //  bucle infinito
	while(1){
		VDP_waitVSync();
	}

    return 0;
}
