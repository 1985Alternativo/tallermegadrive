#include "main.h"
#include "snd/psg/sfxbank.h"

////////////////////////////////////////////////////////////////////////////////
//
// int main()
//
////////////////////////////////////////////////////////////////////////////////
int main(){
	
	// paleta para descompresion
	u16 palB[16];
	
	// para la carga en vram de tiles al descomprimir
	u16 size;  

	// tamaño y posicion de los scrolls del mapa grande
    int mapSizeX, mapSizeY, scrollPosXPlanA, scrollPosXPlanB;

	// cambio de direccion del scroll del mapa grande
	u8 exit = 0;
	
	VDP_regInit();
	
	// cambia el color del fondo
    VDP_setReg( 7, 0x0F );

	// definimos el plano window
    VDP_setReg(17, 0x00);
    VDP_setReg(18, 0x03);

	// carga el driver XGM en el z80
    SND_loadDriver_XGM();
	
	// carga de los tiles de la fuente
	VDP_loadTileData( font00_tiles, pos_vram_font, font_tiles_size, 1);
	
    ////////////////////////////////////////
    //  bucle infinito
	while(1){
		
		/////////////////////////////////////////
        {// dibujo de 1 tile
        VDP_clearPlan(APLAN);
        VDP_clearPlan(BPLAN);
        VDP_clearPlan(WPLAN);
		
		VDP_setPalette((u16 *)palette_black, 0, 16);
		VDP_setPalette((u16 *)palette_black, 16, 16);
		VDP_setPalette((u16 *)palette_black, 32, 16);
		
		VDP_drawText("dibujo de un tile",BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 11, 1 );

        // rellena un rectangulo de 25x18 tiles del tile indicado con paleta 0 en (8,5)
		for( exit = 0; exit < 18; exit++ )
			VDP_setTile(BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0, pos_vram_font+16+exit), 10+exit, 5+exit );
		
		VDP_setPalette((u16 *)font00_pal, 0, 16);

        // espera
        delay(180);}
		
		/////////////////////////////////////////
        {// relleno usando 1 tile
        VDP_clearPlan(APLAN);
        VDP_clearPlan(BPLAN);
        VDP_clearPlan(WPLAN);
		
		VDP_setPalette((u16 *)palette_black, 0, 16);
		
		VDP_drawText("relleno de un tile",BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 11, 1 );

        // rellena un rectangulo de 25x18 tiles del tile indicado con paleta 0 en (8,5)
        VDP_fillTileRect(BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0, pos_vram_font+17), 8, 5, 25, 18);
		
		VDP_setPalette((u16 *)font00_pal, 0, 16);

        // espera
        delay(180);}
		
		////////////////////////////////////////////////////////
        {// relleno usando tiles secuenciales
        VDP_clearPlan(APLAN);
        VDP_clearPlan(BPLAN);
        VDP_clearPlan(WPLAN);
		
		VDP_setPalette((u16 *)palette_black, 0, 16);

		VDP_drawText("relleno de tiles secuenciales", BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 6, 1 );

		// cargo los tiles comprimidos de "example2_data" en la vram y su paleta
        size = depack( example2_data, temp );
		
		VDP_vramCopy( 32, &temp[16], size-32 ); // dir destino= tile*32, origen, tamaño
		VDP_unpackPalette( palB );

		 // dibujo en el plano indicado, atributos de tile, x, y, ancho, alto 
		VDP_fillTileRectInc( BPLAN, TILE_ATTR_FULL(PAL1, 1, 0, 0, 1), 8, 5, 25, 18);
		
		VDP_setPalette((u16 *)font00_pal, 0, 16);
		VDP_setPalette((u16 *)palB, 16, 16);

		// espera
        delay(180);}
		
		/////////////////////////////////////////////////////////////
        {// relleno de un rectangulo con un mapa de tiles 
        VDP_clearPlan(APLAN);
        VDP_clearPlan(BPLAN);
        VDP_clearPlan(WPLAN);
		
		VDP_setPalette((u16 *)palette_black, 0, 16);
		VDP_setPalette((u16 *)palette_black, 16, 16);

        // cargo los tiles comprimidos de "example2_data" en la vram y su paleta
        size = depack( logo1985_data, temp );
		
		VDP_vramCopy( 32, &temp[16], size-32 ); // dir destino= tile*32, origen, tamaño
		VDP_unpackPalette( palB );

        // dibujo el mapa de tiles en el plano indicado, x, y, ancho, alto, atributos de tile
		VDP_fillTileMap( BPLAN, logo1985_map, 12, 6, 16, 10, TILE_ATTR_FULL(PAL1, 1, 0, 0, 2)); 
		
		VDP_drawText("relleno de un mapa de tiles",BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 6, 1 );
		
		VDP_setPalette((u16 *)font00_pal, 0, 16);
		VDP_setPalette((u16 *)palB, 16, 16);
		
        // espera
        delay(180);}
		
		/////////////////////////////////////////////////////////////////////////
        {// relleno de un mapa de tiles secuenciales GRANDE
        VDP_clearPlan(APLAN);
        VDP_clearPlan(BPLAN);
        VDP_clearPlan(WPLAN);

        // cargo los tiles comprimidos del fondo A y B en la vram 
        VDP_loadTileData( planB_tiles, 1, planB_tiles_size, 1);
		VDP_loadTileData( planA_tiles, 1+planB_tiles_size, planA_tiles_size, 1);

        // dibujo el mapa de tiles en el plano indicado, x, y, ancho, alto, atributos de tile 
		VDP_fillTileMap( BPLAN, planB_map, 0, 0, 64, 28, TILE_ATTR_FULL(PAL1, 1, 0, 0, 1));

        VDP_drawText("relleno de un mapa de tiles grande", WPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 3, 1 );

        // tamaño del mapa en tiles
        mapSizeX = 256;
        mapSizeY = 32;

        // posicion del scroll
        scrollPosXPlanA = scrollPosXPlanB = 0;

        // muevo el scroll hasta la parte inferior del mapa
        VDP_setVerticalScroll( APLAN, 0, mapSizeY );

        // dibuja el mapa de tiles desde x1, y1 a x2, y2, del ancho total en tiles, atributos de tile
        VDP_fillBigTileMap( APLAN, planA_map, 0, 0, screen_width_tiles, mapSizeY, mapSizeX, TILE_ATTR_FULL(PAL2, 1, 0, 0, 1+planB_tiles_size));
		
		VDP_setPalette((u16 *)font00_pal, 0, 16);
		VDP_setPalette((u16 *)planB_pal, 16, 16);
		VDP_setPalette((u16 *)planA_pal, 32, 16);

        // mueve el fondo a la derecha
		exit = 0;
        while( !exit ){

			if( scrollPosXPlanA < (mapSizeX<<3)-screen_width ){

                // incremento o decremento la posicion
                scrollPosXPlanA += 4;
                scrollPosXPlanB += 2;

                // dibujo la parte de fuera de la pantalla por la derecha si voy hacia la derecha
                VDP_updateBigTileMap( APLAN, planA_map, (scrollPosXPlanA>>3)+screen_width_tiles, 0, (scrollPosXPlanA>>3)+screen_width_tiles, mapSizeY, mapSizeX, TILE_ATTR_FULL(PAL2, 1, 0, 0, 1+planB_tiles_size));

                // hace el scroll
                VDP_setHorizontalScroll(APLAN, 0, -scrollPosXPlanA);
                VDP_setHorizontalScroll(BPLAN, 0, -scrollPosXPlanB);
            }
			else
                exit = 1;

            VDP_waitVSync();
        }
        exit = 0;

        // mueve el fondo a la izquierda
        while( !exit ){

            if( scrollPosXPlanA > 0 ){

                // incremento o decremento la posicion
                scrollPosXPlanA -= 4;
                scrollPosXPlanB -= 2;

                // dibujo la parte de fuera de la pantalla por la izquierda si voy hacia la izquierda
                VDP_updateBigTileMap( APLAN, planA_map, scrollPosXPlanA>>3, 0, scrollPosXPlanA>>3, mapSizeY, 256, TILE_ATTR_FULL(PAL2, 1, 0, 0, 1+planB_tiles_size));

                // hace el scroll
                VDP_setHorizontalScroll(APLAN, 0, -scrollPosXPlanA);
                VDP_setHorizontalScroll(BPLAN, 0, -scrollPosXPlanB);
            }
			else
                exit = 1;

            VDP_waitVSync();
        }
        exit = 0;}
	}

    return 0;
}
