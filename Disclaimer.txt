MINILIBS is an open and free development kit for the Sega Megadrive.
It contains a development library (sources included) to make software on Sega Megadrive / Genesis system 
and a GCC compiler toolchains (Windows system only). Its code is based on:

- PSG Driver by Shiru (https://shiru.untergrund.net/)

- SGDK by Steff (https://github.com/Stephane-D/SGDK)

- Chilly Willy (chilly willy in http://gendev.spritesmind.net/forum)

MINILIBS and affiliated tools are distribued under the MIT license (https://opensource.org/licenses/MIT)